FROM fedora
RUN dnf install -y \
    inkscape \
    && dnf clean all \
  	&& rm -rf /var/cache/yum